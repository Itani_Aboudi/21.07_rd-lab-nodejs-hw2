const jwt = require('jsonwebtoken');

const authMiddleware = (req, res, next)  => {
    const {
        authorization
    } = req.headers;

    if(!authorization) {
        return res.status(401).json({message: 'Authorization required'})
    }

    const [, token] = authorization.split(' ');

    if(!token) {
        return res.status(401).json({message: 'Token required'})
    }

    try{
        const tokenPayload = jwt.verify(token, 'secret');
        req.user = {
            userId: tokenPayload._id,
            email: tokenPayload.email
        };
        next();
    }catch (err) {
        return res.status(401).json({message: err.message});
    }
};

module.exports = {
    authMiddleware
}