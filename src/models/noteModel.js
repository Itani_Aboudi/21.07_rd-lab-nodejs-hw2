const mongoose = require('mongoose');

const Note = mongoose.model('Notes', {
    title: {
        type: String,
        required: true
    },
    text: {
        type: String,
        required: true
    },
    author: String,

    userId: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },

    createdAt: {
        type: Date,
        default: Date.now()
    }
});

module.exports = {Note}