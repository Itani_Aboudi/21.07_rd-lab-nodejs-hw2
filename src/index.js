const express = require('express');
const path = require('path');
const morgan = require('morgan');
const mongoose = require('mongoose');
const app = express();

const {notesRouter} = require('./controllers/notesController');
const {authRouter} = require('./controllers/authController');
const {authMiddleware} = require('./middlewares/authMiddleware');

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);

app.use(authMiddleware);
app.use('/api/notes', notesRouter);

const start = async () => {
    try {
        await mongoose.connect('mongodb+srv://Aboudi:!panda1309!@cluster0.9hc56.mongodb.net/notesApp?retryWrites=true&w=majority', {
            useNewUrlParser: true, useUnifiedTopology: true
        });

        app.listen(8080);
    } catch (err) {
        console.error(`Error on start: ${err.message}`);
    }
}

start();