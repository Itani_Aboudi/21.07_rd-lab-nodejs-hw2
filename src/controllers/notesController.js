const express = require('express');
const router = express.Router();

const  {
    getNotesByUserId,
    addNoteToUser
} = require('../services/noteServices');

router.get('/', async (req, res)=>{
    const {userId} = req.user;

    console.log(req.user);

    const notes = await getNotesByUserId(userId);

    res.json({notes});
});

router.post('/', async (req, res)=>{
    const {userId} = req.user;
    const notes = await addNoteToUser(userId, req.body);

    res.json({messages: 'Note created successfully'});
});

module.exports = {
    notesRouter: router
}