const express = require('express');
const router = express.Router();

const {
    registration,
    sigIn
} = require('../services/authServise');

router.post('/registration', async (req, res) => {
   try{
       const {
           email,
           password
       } = req.body;

       await registration({email, password});

       res.json({massage: "Account created successfully"});
   } catch (err) {
       res.status(500).json({message: err.message});
   }

});

router.get('/signin', async (req, res) => {
    try{
        const {
            email,
            password
        } = req.body;

        const token = await sigIn({email, password});

        res.json( {token, massage: "Logged in successfully"});
    } catch (err) {
        res.status(500).json({message: err.message});
    }
});

module.exports = {
    authRouter: router
}